#include <stdlib.h>
#include <iostream>

class stos_2
{
	int* _stack;
public:
	stos_2(){size=10; _stack = new int[size]; end=0;}
	int  size;
  int  end;           //w zasadzie ilosc elementow, _stack[end] jest pusty
  void big();
  void push(int a);
  int  pop();
  void print();
  void nuke() {delete[] _stack; size=10; _stack= new int[size]; end=0;}
  bool empty() {return end==0 ? true:false;}
};

void stos_2::big()
{
	int* temp;
  size=size*2;
  temp=new int[size];
  for(int i=0;i<end;i++)
    temp[i] = _stack[i];
  delete[] _stack;
  _stack = temp;
}

void stos_2::push(int a)
{
  if(size==end)
    big();
  _stack[end++]=a;
}

int stos_2::pop()
{ 
	if(end!=0)
    return _stack[--end];
}

void stos_2::print()
{
  for(int i=0;i<end;i++)
    std::cout << _stack[i] << ' ';
}

stos_2 wieza[3];

bool move(int skad, int gdzie)
{
	int temp;
	if(wieza[skad].empty())
    return 0;
  wieza[gdzie].push(wieza[skad].pop());
  return 1;
}

void print_wieze()
{
	for(int i=0; i<3;i++)
  {
	  std::cout << "I ";
    wieza[i].print();
    std::cout << '\n';
  }
  std::cout << '\n';
}

void przeloz(int n, int A, int B, int C)
{
	if (n>0)
  {
	  przeloz(n-1, A, C, B);    
	  move(A, C);
    print_wieze();
    przeloz(n-1, B, A, C);
  }
}

int main()
{
	int n=10;
  for(int i=0;i<n;i++)
    wieza[0].push(n-i);
  
  print_wieze();
  przeloz(n, 0, 1, 2);
  return 0;
}

