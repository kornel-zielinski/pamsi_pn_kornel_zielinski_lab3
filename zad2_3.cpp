#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <stack>
#include <windows.h>
#define INCREMENT 10

class stos_2
{
	int* _stack;
public:
	stos_2(){size=1; _stack = new int[size]; end=0;}
	int  size;
  int  end;           //w zasadzie ilosc elementow, _stack[end] jest pusty
  void big();
  void push(int a);
  int  pop();
  void print();
  void nuke() {delete[] _stack; size=1; _stack= new int[size]; end=0;}
};

void stos_2::big()
{
	int* temp;
  size=size*2;
  temp=new int[size];
  for(int i=0;i<end;i++)
    temp[i] = _stack[i];
  delete[] _stack;
  _stack = temp;
}

void stos_2::push(int a)
{
  if(size==end)
    big();
  _stack[end++]=a;
}

int stos_2::pop()
{ 
	if(end!=0)
    return _stack[--end];
}

void stos_2::print()
{
  for(int i=end;i>0;i--)
    std::cout << _stack[i-1] << '\n';
}

class stos_1
{
	int* _stack;
public:
	stos_1(){size=1; _stack = new int[size]; end=0;}
	int  size;
  int  end;           //w zasadzie ilosc elementow, _stack[end] jest pusty
  void big();
  void push(int a);
  int  pop();
  void print();
  void nuke() {delete[] _stack; size=1; _stack= new int[size]; end=0;}
};

void stos_1::big()
{
	int* temp;
  size=size+INCREMENT;
  temp=new int[size];
  for(int i=0;i<end;i++)
    temp[i] = _stack[i];
  delete[] _stack;
  _stack = temp;
}

void stos_1::push(int a)
{
  if(size==end)
    big();
  _stack[end++]=a;
}

int stos_1::pop()
{ 
	if(end!=0)
    return _stack[--end];
}

void stos_1::print()
{
  for(int i=end;i>0;i--)
    std::cout << _stack[i-1] << '\n';
}

LARGE_INTEGER startTimer()
{
LARGE_INTEGER start;
DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
QueryPerformanceCounter(&start);
 SetThreadAffinityMask(GetCurrentThread(), oldmask);
 return start;
}

LARGE_INTEGER endTimer()
{
  LARGE_INTEGER stop;
  DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
  QueryPerformanceCounter(&stop);
  SetThreadAffinityMask(GetCurrentThread(), oldmask);
  return stop;
}

int main()
{
  stos_1 inc;
  stos_2 mul;
  std::stack<int> sst;
  LARGE_INTEGER start, end;
  int n[]={1000, 10000, 100000, 500000};

  std::cout << "Czas potrzebny na dodanie n liczb do stosu powiekszanego przez podwajanie\n";
  for(int i=0;i<4;i++)
  {  
    start=startTimer();
    for(int j=0;j<n[i];j++)
      mul.push(i);
    end=endTimer();
    std::cout  << "n=" << n[i] << " " << end.QuadPart-start.QuadPart<< std::endl;
    mul.nuke();
  }

  std::cout << "Czas potrzebny na dodanie n liczb do stosu powiekszanego inkrementalnie\n";
  for(int i=0;i<4;i++)
  {  
    start=startTimer();
    for(int j=0;j<n[i];j++)
      inc.push(i);
    end=endTimer();
    std::cout  << "n=" << n[i] << " " << end.QuadPart-start.QuadPart<< std::endl;
    inc.nuke();
  }
  std::cout << "Czas potrzebny na dodanie n liczb do stosu z STL\n";
  for(int i=0;i<4;i++)
  {  
    start=startTimer();
    for(int j=0;j<n[i];j++)
      sst.push(i);
    end=endTimer();
    std::cout  << "n=" << n[i] << " " << end.QuadPart-start.QuadPart<< std::endl;
    while(!sst.empty()) 
      sst.pop();
  }
  system("pause");
  return 0;
}
